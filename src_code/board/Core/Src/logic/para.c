#include "para.h"
#include "iniparser.h"
#include "bsp_w25q64.h"

System_Param device_system_para ;
System_Param device_system_para_cpy ;

/*加载配置参数*/
void Load_Config_Para(void)
{
    uint8_t i = 0 ;
    W25QXX_Read((uint8_t *)&device_system_para_cpy, 0, sizeof(System_Param));
    /*将数据重新拷贝一份*/
    memcpy((uint8_t *)&device_system_para, (uint8_t *)&device_system_para_cpy, sizeof(System_Param));

    /*进行初始化设置*/
    if(0 == device_system_para.init_flag || 255 == device_system_para.init_flag)
    {
        W25QXX_Erase_Sector(0);
        device_system_para.init_flag = 1 ;
        device_system_para.alarm = 1 ;
        device_system_para.value = 1 ;
        device_system_para.sensivity = 1 ;
        device_system_para.debug_flag = 1 ;
        device_system_para.upload_flag = 0 ;

        for(i = 0 ; i < 4 ; i++)
            device_system_para.password[i] = 0 ;

        device_system_para.detect_log_serial_number = 0 ;
        device_system_para.alarm_threshold[0] = 1000 ;
        device_system_para.alarm_threshold[1] = 600 ;
        device_system_para.alarm_threshold[2] = 350 ;
        W25QXX_Page_Program((uint8_t *)&device_system_para, 0, sizeof(device_system_para));
        W25QXX_Read((uint8_t *)&device_system_para_cpy, 0, sizeof(System_Param));
    }
}

/*设置调试*/
void setting_debug(uint8_t *status)
{
    device_system_para.debug_flag = *status ;
}

/*设置音量*/
void setting_alarm(uint8_t *status)
{
    device_system_para.alarm = *status ;
}

/*设置数值显示*/
void setting_value(uint8_t *status)
{
    device_system_para.value = *status ;
}

/*设置网络显示*/
void setting_network(uint8_t *status)
{
    device_system_para.upload_flag = *status ;
}

/*设置灵敏度*/
void setting_sensivity(uint8_t *status)
{
    device_system_para.sensivity = *status ;
}


/*设置设备密码*/
void setting_device_password(System_Param para)
{
    uint8_t i = 0 ;

    for(i = 0 ; i < 4 ; i++)
        device_system_para.password[i] = para.password[i];
}

/*用户保存参数*/
void User_Save_Para(void)
{
    tos_knl_sched_lock();
    W25QXX_Erase_Sector(0);
    W25QXX_Page_Program((uint8_t *)&device_system_para, 0, sizeof(System_Param));
    memset((uint8_t *)&device_system_para_cpy, 0, sizeof(System_Param));
    W25QXX_Read((uint8_t *)&device_system_para_cpy, 0, sizeof(System_Param));
    tos_knl_sched_unlock();
}
