#include "app_init.h"
#include "adc.h"
#include "fatfs.h"
#include "rtc.h"
#include "sdmmc.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "bsp_bmp.h"
#include "quadspi.h"


FATFS fs;
FRESULT f_res;
#define START_LOGO	"0:/UI/start_logo/start_logo_for_tencent.bmp"
#define WLAN_LOGO0 	"0:/UI/status_bar/connect_wifi.bmp"
#define WLAN_LOGO1 	"0:/UI/status_bar/connect_server.bmp"
LCD_Driver_Model lcd_model ;

/*默认任务*/
#define DEFULT_TASK_SIZE		  256
void default_task(void *pdata);
osThreadDef(default_task, osPriorityLow, 1, DEFULT_TASK_SIZE);

/*按键处理*/
#define KEY_TASK_SIZE				  1500
void StartKeyTask(void  *argument);
osThreadDef(StartKeyTask, osPriorityRealtime, 1, KEY_TASK_SIZE);

/*状态栏任务处理*/
#define STATUS_BAR_TASK_SIZE	400
void StartStatus_Bar_Task(void  *argument);
osThreadDef(StartStatus_Bar_Task, osPriorityRealtime, 1, STATUS_BAR_TASK_SIZE);

/*传感器任务处理*/
#define SENSOR_TASK_SIZE			512
void StartSensor_Task(void  *argument);
osThreadDef(StartSensor_Task, osPriorityHigh, 1, SENSOR_TASK_SIZE);

/*网络数据上传任务处理*/
uint8_t esp_8266_data;

LCD_Bmp_Show_Para boot_logo_para = {0, 0, START_LOGO};


LCD_Bmp_Show_Para status_wlan_para[] =
{
    {2, 5, WLAN_LOGO0},
    {35, 5, WLAN_LOGO1},
};

/*显示无线状态*/
void display_wlan_status(uint8_t status)
{
    LCD_Fill_Para fill_logo;
    fill_logo.x_start = 2 ;
    fill_logo.y_start = 5 ;
    fill_logo.color   = BLACK ;
    fill_logo.x_end = 2 + 24 ;
    fill_logo.y_end = 5 + 24 ;

    if(0 == status)
        lcd_model.lcd_driver->lcd_fill(fill_logo);
    else if(1 == status)
        lcd_model.lcd_driver->Lcd_show_bmp(status_wlan_para[0]);
    else if(2 == status)
        lcd_model.lcd_driver->Lcd_show_bmp(status_wlan_para[1]);
}


int $Sub$$main(void)
{
    extern int main(void);
    extern int $Super$$main(void);
    int ret = -1 ;
    HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();
    MX_SPI2_Init();
    MX_USART1_UART_Init();
    MX_QUADSPI_Init();
    MX_RTC_Init();
    MX_ADC1_Init();
    MX_TIM16_Init();
    MX_SDMMC1_SD_Init();
    MX_FATFS_Init();
    Register_Driver_Model(&lcd_model);
    //PowerOn();
    lcd_model.lcd_driver->lcd_init();
    ret = Mount_SD();

    if(ret != 0)
    {
        printf("SD Card mount ERROR\r\n");
        HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
        return -1 ;
    }

    /*读取配置信息*/
    Load_Config_Para();
    lcd_model.lcd_driver->Lcd_show_bmp(boot_logo_para);
    lcd_model.lcd_driver->lcd_display_onoff(1);
    HAL_Delay(1500);
    Sensor_Register(&mq2_sensor_interface);
    lcd_model.lcd_driver->lcd_clear(BLACK);
    Menu_Init();
    display_tencent_logo(1);
    //关指示灯
    HAL_GPIO_WritePin(GPIOC, LED_Pin, GPIO_PIN_RESET);
    //回到真正的main函数里
    $Super$$main();
    return 0 ;
}

/*启动操作系统*/
void start_tencent_os(void)
{
    /*初始化腾讯OS tiny内核*/
    osKernelInitialize();
    /*创建并启动一个默认任务*/
    osThreadCreate(osThread(default_task), NULL);
    /*启动TencentOS tiny内核*/
    osKernelStart();
}


/*默认任务处理*/
void default_task(void *pdata)
{
    tos_knl_sched_lock();
    /*创建按键任务*/
    osThreadCreate(osThread(StartKeyTask), NULL);
    /*创建状态栏任务*/
    osThreadCreate(osThread(StartStatus_Bar_Task), NULL);
    /*创建传感器数据任务*/
    osThreadCreate(osThread(StartSensor_Task), NULL);
    tos_knl_sched_unlock();
}

/*传感器任务处理*/
void StartSensor_Task(void  *argument)
{
    uint32_t i = 0 ;

    while(1)
    {
        mq2_sensor_interface.get_smoke_value(&mq2_sensor_interface);
        /*更新数据到队列*/
        for(i = 0 ; i <= DATA_SIZE - 2 ; i++)
            plot_handler.rel_data_data[i] = plot_handler.rel_data_data[i + 1];

        plot_handler.rel_data_data[DATA_SIZE - 1] = mq2_sensor_interface.Smoke_Value ;

        if(1 == plot_handler.plot_mode && 0 == plot_handler.fixed_screen)
        {
            tos_knl_sched_lock();
            LCD_Plot_Display(plot_handler.old_plot_data, DATA_SIZE, BLACK);
            LCD_Plot_Remap(plot_handler.rel_data_data, plot_handler.new_plot_data, DATA_SIZE);
            LCD_Plot_Display(plot_handler.new_plot_data, DATA_SIZE, GREEN);
            memcpy(plot_handler.old_plot_data, plot_handler.new_plot_data, sizeof(plot_handler.new_plot_data));
            tos_knl_sched_unlock();
        }

        tos_sleep_ms(20);
    }
}

/*按键任务处理*/
void StartKeyTask(void *argument)
{
    int Count_Timer = 0 ;
    __IO uint8_t KeyCode = 255;

    while(1)
    {
        /*获取键值*/
        GetKey(&KeyCode);

        if(255 != KeyCode)
        {
            Menu_Select_Item(Flow_Cursor.flow_cursor, KeyCode);
            KeyCode = 255 ;
        }

        /*如果当前在测试页面 && 开始检测标志为1，则进入传感器数据处理*/
        if(Flow_Cursor.flow_cursor == TEST_PAGE && \
                Sensor_Flow_Cursor.Start_Detect == 1)
        {
            ++Count_Timer ;

            if(Count_Timer == 20)
            {
                Count_Timer = 0 ;
                Sensor_Handler(Sensor_Flow_Cursor.Detect_Step, \
                               mq2_sensor_interface.Smoke_Value);
            }
        }

        tos_sleep_ms(5);
    }
}

/*状态栏任务显示处理*/
extern LCD_Ascii_Show_Para datatime_display_para ;
void StartStatus_Bar_Task(void *argument)
{
    while(1)
    {
        Get_Date_Time();

        if(0 == plot_handler.plot_mode)
        {
            snprintf(DateTime_Handler_Info.DisPlay_DateBuf, 					\
                     sizeof(DateTime_Handler_Info.DisPlay_DateBuf), 	\
                     "%02d:%02d:%02d", DateTime_Handler_Info.hour, 		\
                     DateTime_Handler_Info.minute, DateTime_Handler_Info.sec);
            lcd_model.lcd_driver->lcd_show_ascii_str(datatime_display_para);
        }

        tos_sleep_ms(1000);
    }
}

