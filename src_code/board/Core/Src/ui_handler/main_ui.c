#include "main_ui.h"
#include "bsp_bmp.h"

Main_Page_Ui main_page_ui ;
LCD_Fill_Para LOGO_Fill_Para = {55, 183, 45, 183, BLACK} ;
LCD_Fill_Para status_bar_fill_para = {0, 240, 0, 27, WHITE} ;
LCD_Ascii_Show_Para datatime_display_para = {70, 3, 170, DateTime_Handler_Info.DisPlay_DateBuf, WHITE, BLACK, 24} ;
LCD_Bmp_Show_Para main_logo_para[] =
{
    {55, 45, "0:/UI/main_page/detect_logo.bmp"},
    {55, 45, "0:/UI/main_page/log_logo.bmp"},
    {55, 45, "0:/UI/main_page/conf_logo.bmp"},
};

LCD_Chinese_Show_Para main_title_font[] =
{
    {40, 180, (uint8_t *)"危险气体探测仪", WHITE, BLACK, 24, 1},
    {40, 180, (uint8_t *)"危险气体探测仪", BLACK, BLACK, 24, 1},
};

#define START_LOGO1 "0:/UI/start_logo/BearPi1.bmp"
LCD_Bmp_Show_Para boot_logo1_para = {54, 207, START_LOGO1};

/*选择菜单项*/
void Select_Main_Menu_Item(uint8_t item)
{
    if(0 == item)
        lcd_model.lcd_driver->Lcd_show_bmp(main_logo_para[0]);
    else if(1 == item)
        lcd_model.lcd_driver->Lcd_show_bmp(main_logo_para[1]);
    else if(2 == item)
        lcd_model.lcd_driver->Lcd_show_bmp(main_logo_para[2]);
}

/*获取RTC时钟并显示*/
void Get_RTC_Date_Time(void)
{
    Get_Date_Time();
    snprintf(DateTime_Handler_Info.DisPlay_DateBuf, sizeof(DateTime_Handler_Info.DisPlay_DateBuf), \
             "%02d:%02d:%02d", DateTime_Handler_Info.hour, DateTime_Handler_Info.minute, \
             DateTime_Handler_Info.sec);
    lcd_model.lcd_driver->lcd_show_ascii_str(datatime_display_para);
}

/*显示TencentOS tiny logo*/
void display_tencent_logo(uint8_t status)
{
    LCD_Fill_Para fill_logo;
    fill_logo.x_start = 54 ;
    fill_logo.y_start = 207 ;
    fill_logo.color   = BLACK ;
    fill_logo.x_end = fill_logo.x_start + 130 ;
    fill_logo.y_end = fill_logo.y_start + 26 ;

    if(1 == status)
    {
        lcd_model.lcd_driver->Lcd_show_bmp(boot_logo1_para);
        lcd_model.lcd_driver->lcd_show_chinese_str(main_title_font[0]);
    }
    else
    {
        lcd_model.lcd_driver->lcd_fill(fill_logo);
        lcd_model.lcd_driver->lcd_show_chinese_str(main_title_font[1]);
    }
}

/*画框图*/
void draw_rect_func(uint8_t enable)
{
    if(1 == enable)
    {
        LCD_Rect_Show_Para lcd_rect = {0, 239, 0, 239, WHITE};
        lcd_model.lcd_driver->lcd_draw_rect(lcd_rect);
    }
    else
    {
        LCD_Rect_Show_Para lcd_rect = {0, 239, 0, 239, BLACK};
        lcd_model.lcd_driver->lcd_draw_rect(lcd_rect);
    }
}

/*主页面初始化*/
void main_page_init(void)
{
    main_page_ui.select_item = 0;
    Flow_Cursor.flow_cursor = MAIN_PAGE ;
    lcd_model.lcd_driver->lcd_display_onoff(0);
    draw_rect_func(1);
    lcd_model.lcd_driver->lcd_fill(status_bar_fill_para);
    Get_RTC_Date_Time();
    Select_Main_Menu_Item(main_page_ui.select_item);
    lcd_model.lcd_driver->lcd_display_onoff(1);
}


/********************************************按键处理****************************************************/
/*左键处理*/
static void Handler_Main_Page_Left_Key(void)
{
    (main_page_ui.select_item < 2) ? \
    (main_page_ui.select_item++) : 	 \
    (main_page_ui.select_item = 0);
    Select_Main_Menu_Item(main_page_ui.select_item);
}

/*右键处理*/
static void Handler_Main_Page_Right_Key(void)
{
    lcd_model.lcd_driver->lcd_fill(LOGO_Fill_Para);

    if(0 == main_page_ui.select_item)
    {
        display_tencent_logo(0);
        test_page_init();
    }
    else if(1 == main_page_ui.select_item)
    {
        display_tencent_logo(0);
        log_page_init();
    }
    else if(2 == main_page_ui.select_item)
    {
        display_tencent_logo(0);
        password_input_page_init();
    }
}

Event_Frame Main_Page_Event[] =
{
    {Handler_Main_Page_Left_Key},
    {NULL},
    {Handler_Main_Page_Right_Key},
    {NULL},
};

/*主页面事件处理*/
void main_page_process(uint8_t Event_Code)
{
    if(Main_Page_Event[Event_Code - 1].handler_func != NULL)
        Main_Page_Event[Event_Code - 1].handler_func();
}
/********************************************按键处理****************************************************/
